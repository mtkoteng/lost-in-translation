import './App.css';
import Login from "./components/login/Login";
import Profile from "./components/profile/Profile"
import MainPage from "./components/mainpage/MainPage";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Navbar from "./components/navbar/Navbar";

function App() {
  return (
      <BrowserRouter>
          <div className="App">
              <Navbar/>
              <Switch>
                  <Route path="/" exact component={Login}/>
                  <Route path="/main" component={MainPage}/>
                  <Route path="/profile" component={Profile}/>
              </Switch>
          </div>
      </BrowserRouter>
  );
}

export default App;
