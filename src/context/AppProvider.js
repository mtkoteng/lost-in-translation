import {createContext, useState} from "react";
// Adhere to the Everything is a component.
// Create a Context -> Easily Share some data between (Unrelated) components.

export const AppContext = createContext({})

function AppProvider({ children }) {
    // Default we define here.

    const [ username, setUsername ] = useState('')
    const [ translations, setTranslations ] = useState([])

    const AppState = {
        // User
        username,
        setUsername,
        // Translations
        translations,
        setTranslations
    }

    return (
        <AppContext.Provider value={ AppState }>
            { children }
        </AppContext.Provider>
    )
}

export default AppProvider