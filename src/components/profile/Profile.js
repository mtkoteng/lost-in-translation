import {LocalStorage} from "../storage/Storage";
import './Profile.css'
import {Redirect, useHistory} from "react-router-dom"
import {useState, useContext} from "react";
import {AppContext} from "../../context/AppProvider";

function Profile(){
    const history = useHistory()
    const [searched, setSearched] = useState(LocalStorage.get('searches') || [])
    const {username, setUsername} = useContext(AppContext)

    //Logout function. Clears Localstorage and state. Sends user to login page.
    const logout = () => {
        history.push('/')
        LocalStorage.clear()
        setUsername('')
    }
    //Removes translations from Localstorage and resets searched array.
    const deleteTranslations = () => {
        LocalStorage.remove('searches')
        setSearched([])
    }
    //Displays table where new elements is ordered descending in a list.
    const displayTable = () => {
        return searched.reverse().map((search, index) => {
            return (
                <li key={index} className="list-item"> {search} </li>
            )
        })
    }
    return (
        <div>
            { !username && !LocalStorage.get('username') && <Redirect to="/" /> }
            <div className="profile-page">
                <h2>Profile page</h2>
                <div className="list-container">
                    <p>Last {searched.length} searches</p>
                    <ul className="list">
                        { displayTable() }
                    </ul>
                    <button onClick={deleteTranslations} className="btn">Delete translations</button>
                </div>
                <div>
                    <button onClick={logout} className="logout-btn">Log out</button>
                </div>
            </div>
        </div>
    )
}
export default Profile