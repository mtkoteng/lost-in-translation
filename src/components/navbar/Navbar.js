import {NavLink} from "react-router-dom";
import './Navbar.css'
import {useContext} from "react";
import {AppContext} from "../../context/AppProvider";
import {LocalStorage} from "../storage/Storage";

function Navbar (){
    const {username, setUsername} = useContext(AppContext)

    //Checks if the state.username has a value, otherwise sets username in state to Localstorage.get('username'). User can refresh and get the old state from Localstorage.
    function hasUsername(){
        if(username)
        {
            return username
        }
        else{
            setUsername(LocalStorage.get('username'))
        }
    }

    return(
        <nav className="navbar">
            <ul className="navbar-list">
                <li className="title"><NavLink to='/main' className='navbar-link'>
                    {username && 'Lost in Translation'}
                </NavLink></li>
                <li className="profile"><NavLink to='/profile'> {hasUsername()} </NavLink></li>
            </ul>
        </nav>
    )
}
export default Navbar