
export const LocalStorage = {
    get(key){
        const stored = localStorage.getItem(key)
        if (!stored){
            return null
        }
        try {
            const dec = atob(stored)
            return JSON.parse(dec)
        } catch (e){
            return null
        }
    },
    set(key, value) {
        try {
            const json = JSON.stringify(value)
            const enc = btoa(json)
            localStorage.setItem(key, enc)
        } catch (e) {
            console.log(e.message)
        }
    },
    remove(key){
        const stored = localStorage.getItem(key)
        if (stored){
            localStorage.removeItem(key)
            return true
        }
        return false
    },
    clear(){
        localStorage.clear()
    }
}