import './login.css'
import logo from '../../assets/Logo-Hello.png'
import {useState, useContext} from "react";
import {useHistory} from "react-router-dom"
import {LocalStorage} from "../storage/Storage";
import {AppContext} from "../../context/AppProvider";

function Login() {

    // Localstate = username
    const [ localUsername, setLocalUsername ] = useState('')

    // Global state
    const { setUsername } = useContext(AppContext)


    // Import useContext hook
    // Destructure the username and setUsername from AppState <- AppContext

    const history = useHistory()

    const enterName = e => setLocalUsername(e.target.value)

    const enterSite = () => {
        // Update Global state.
        setUsername( localUsername )
        LocalStorage.set('username', localUsername)
        history.push('/main')
    }
    return(
        <div className="container">
            <div className="login">
                <div className="login-top">
                    <div className="icon">
                        <img src={logo} className="picture" alt="Logo"/>
                    </div>
                    <div className="welcome-message">
                        <h1>Lost in translation</h1>
                        <h3>Please enter name to get started!</h3>
                    </div>
                </div>
                <div className="login-bottom">
                </div>
            </div>
            <div className="overlay">
                <input onChange={enterName} className="enter-name" type="text" placeholder="What is your name?"/>
                <button onClick={enterSite} className="enter-button">Go!</button>
            </div>
        </div>
    )
}

export default Login