import './SearchForm.css'
import {useState} from 'react'
import ResultWindow from "./ResultWindow";
import {LocalStorage} from "../storage/Storage";
function SearchForm() {

    const [search, setSearch] = useState([])
    const [savedSearches, setSavedSearches] = useState(LocalStorage.get('searches') || [])

    //Checks if the values in search field is of correct format. Uses regex only a-z allowed.
    const isLetter = (str) => {
        for (let i = 0; i < str.length; i++){
            if (!str[i].match(/[a-z]/i)){
                return false
            }
        }
        return true
    }

    //Calls for isLetter and saves search if the value has correct format.
    const searchInput = (e) =>{
        if (isLetter(e.target.value)){
            setSearch(e.target.value.split(''))
        } else {
            alert('Invalid input!')
            e.target.value = ''
        }
    }

    //Saves the translation in LocalStorage so that profile can get the search history.
    const showTranslation = () => {
        let searchedFor = search.join("")
        if (searchedFor !== ''){
            let newSearches = [
                ...savedSearches,
                searchedFor
            ]
            if (newSearches.length > 10){
                newSearches.shift()
                LocalStorage.set('searches', newSearches)
                setSavedSearches(newSearches)
            } else {
                LocalStorage.set('searches', newSearches)
                setSavedSearches(newSearches)
            }
            alert('Your search is stored!')
        }
    }

    return(
        <div className="containter">
            <div className="form">
                <input onChange={searchInput} className="search" type="text" placeholder="Search..."/>
                <button onClick={showTranslation} value="Search" className="search-button">Save</button>
            </div>
            <div>
                { search && <ResultWindow search={ search } /> }
            </div>
        </div>

    )
}
export default SearchForm;