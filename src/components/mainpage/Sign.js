function Sign({letter}) {
    //Returns image if letter exists
    return (
        <div>
            { letter && <img src={ require(`../../assets/signs/${letter}.png`).default } alt={ letter } />}
        </div>
    )
}
export default Sign;