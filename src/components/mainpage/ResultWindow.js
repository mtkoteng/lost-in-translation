import Sign from './Sign'
function ResultWindow({search = []}) {
    //Maps search array to array of sign components and display these in the application.
    return(
        <div>
            <div className="signs">
                { search.map((letter, index) => <Sign key={index} letter={letter.toLowerCase()} />)}
            </div>
        </div>
    )
}
export default ResultWindow;

