import SearchForm from './SearchForm'
import {useContext} from "react";
import {AppContext} from "../../context/AppProvider";
import {Redirect} from 'react-router-dom'
import {LocalStorage} from "../storage/Storage";
function MainPage() {
    const {username} = useContext(AppContext)
    //Displays Searchform component as as long as state.username has a value or Localstorage.get('username') has a value. Otherwise redirect user to login page.
    return(
        <div>
            { !username && !LocalStorage.get('username')  && <Redirect to="/" /> }
            <div>
                <SearchForm/>
            </div>
        </div>

    )
}

export default MainPage;