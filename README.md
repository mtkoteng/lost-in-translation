# Lost in translation
## Contibutors
Jakkris Thongma and Markus Thomassen Koteng
## The task
The mission for this task was to create a React application
which translates letters into sign language. The translations
is stored using local storage
## Run
https://mysterious-temple-57422.herokuapp.com/ 
## The solution
The solution is based on that an user have to login by writing
an username. After writing the username, the user will then
be showed a translation page where you can make searches, get the
translations, and store the searches. The profile page shows the
last searches (max 10), and gives the user the opportunity
to delete the searches, or log out.
### Functional components and hooks
This project is programmed using functional components and
hooks to control the states. An advantage with this is that it
is less code compared to classes.
### Local storage
Local storage is used to store the username and the searches locally
on the machine. It will be cleared when log out and the searches
can also be cleared in profile.
### Context API
Was used to handle the global states, so we was sure that the components
updated as it was meant to.
### Router
Router is used to route between the views in this application.
It is also used to redirect to login when the username is empty,
meaning that no user has been logged in.
